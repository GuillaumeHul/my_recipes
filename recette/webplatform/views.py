from django.shortcuts import render, get_object_or_404, redirect
from webplatform.models import Recipe
from webplatform.models import RecipeForm
from webplatform.models import IngredientForm


def home(request):
    """if request.method == 'POST':
        if 'unity' in request.POST:
            form = IngredientForm(request.POST)
        elif 'id' in request.POST:
            form = request.POST
        else:
            form = RecipeForm(request.POST)

        if 'id' in request.POST:
            is_archived = True if 'is_archived' in request.POST else False
            r = get_object_or_404(Recipe, id=form['id'])
            r.is_archived = is_archived
            r.save()
        else:
            if form.is_valid():
                form.save()"""

    recipes = Recipe.objects.filter(is_archived=False)
    archived_recipes = Recipe.objects.filter(is_archived=True)

    recipe_form = RecipeForm()
    ingredient_form = IngredientForm(request.POST or None, instance=Recipe)
    ingredient_form.fields["recipe"].queryset = Recipe.objects.filter(is_archived=False)
    return render(request, 'home/index.html',
                  {
                      'RecipeForm': recipe_form,
                      'IngredientsForm': ingredient_form,
                      'recipes': recipes,
                      'archived_recipes': archived_recipes
                  })


def add_ingredient(request):
    if request.method == 'POST':
        form = IngredientForm(request.POST)
        if form.is_valid():
            form.save()

    return redirect(home)


def add_recipe(request):
    if request.method == 'POST':
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()

    return redirect(home)


def archive_recipe(request):
    if request.method == 'POST':
        form = request.POST
        is_archived = True if 'is_archived' in request.POST else False
        r = get_object_or_404(Recipe, id=form['id'])
        r.is_archived = is_archived
        r.save()

    return redirect(home)
