from django.db import models
from django import forms


class Recipe(models.Model):
    name = models.CharField(max_length=30)
    is_archived = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Unity(models.Model):
    name = models.CharField(max_length=5)

    def __str__(self):
        return self.name


class Ingredients(models.Model):
    name = models.CharField(max_length=30)
    unity = models.ForeignKey(Unity, on_delete=models.DO_NOTHING)
    recipe = models.ManyToManyField(Recipe)
    quantity = models.IntegerField()

    def __str__(self):
        return self.name


class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = ['name']


class IngredientForm(forms.ModelForm):
    class Meta:
        model = Ingredients
        fields = ['name', 'quantity', 'unity', 'recipe']
        recipe = forms.ModelMultipleChoiceField(queryset=Recipe.objects.none())

        def __init__(self, *args, **kwargs):
            qs = kwargs.pop('recipe')
            super().__init__(*args, **kwargs)
            self.fields['unity'].queryset = Unity.objects.all()
            self.fields['recipe'].queryset = qs
