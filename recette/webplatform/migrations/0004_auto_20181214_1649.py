# Generated by Django 2.0 on 2018-12-14 16:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webplatform', '0003_auto_20181214_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='is_archived',
            field=models.BooleanField(default=False),
        ),
    ]
